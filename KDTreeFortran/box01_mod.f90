MODULE box01
    USE def01, ONLY : DP, I4B, ierror
    USE point01
    IMPLICIT NONE
    
    PUBLIC :: box_dist

    TYPE box
        !PRIVATE

        TYPE(point) :: lo, hi
        INTEGER(I4B) :: mom, dau1, dau2, ptlo, pthi

        CONTAINS
            ! Generic names
            !GENERIC, PUBLIC :: ether_init => ether_init1, ether_init2 ! Fortran-style overloading (name binding)
            ! Public procedures
            PROCEDURE, PUBLIC :: box_init
            PROCEDURE, PUBLIC :: boxnode_init
            ! Destructor
            !FINAL :: box_final ! can be omitted
            ! Private procedures
            !PROCEDURE, PRIVATE :: ether_initsps

    END TYPE box


    CONTAINS ! Begin definitions' section

    SUBROUTINE box_init(this, mylo, myhi)
        ! Initialize ether.
        ! Written by F.S. Zaitsev 06.11.2014.
        ! Last modification 06.11.2014.
        !USE def01, ONLY : I4B,DP,pi2
        IMPLICIT NONE
        CLASS(box), INTENT(INOUT) :: this
        TYPE(point), INTENT(IN) :: mylo, myhi

        call point_init(this%lo, size(mylo.x), mylo%x(1), mylo%x(2), mylo%x(3), mylo%x(4), mylo%x(5), mylo%x(6))
        call point_init(this%hi, size(myhi.x), myhi%x(1), myhi%x(2), myhi%x(3), myhi%x(4), myhi%x(5), myhi%x(6))
        
        !this%lo = mylo
        !this%hi = myhi

    END SUBROUTINE box_init

    SUBROUTINE boxnode_init(this, mylo, myhi, mymom, myd1, myd2, myptlo, mypthi)
        ! Initialize ether.
        ! Written by F.S. Zaitsev 06.11.2014.
        ! Last modification 06.11.2014.
        !USE def01, ONLY : I4B,DP,pi2
        IMPLICIT NONE
        CLASS(box), INTENT(INOUT) :: this
        TYPE(point), INTENT(IN) :: mylo, myhi
        INTEGER(I4B) :: mymom, myd1, myd2, myptlo, mypthi

        call box_init(this, mylo, myhi)

        !this%lo = mylo
        !this%hi = myhi
        this%mom = mymom
        this%dau1 = myd1
        this%dau2 = myd2
        this%ptlo = myptlo
        this%pthi = mypthi

    END SUBROUTINE boxnode_init

    !SUBROUTINE box_final(this) ! definition of the destructor
    !  IMPLICIT NONE
    !  TYPE(box), INTENT(INOUT) :: this
    !
    !  if(allocated(this%x)) deallocate(this%x,stat=ierror)
    !
    !END SUBROUTINE ether_final
    
    FUNCTION box_dist(this, point) result(dd)
        IMPLICIT NONE
        
        TYPE(box), INTENT(INOUT) :: this
        REAL(DP), INTENT(IN) :: point(:)
        REAL(DP) :: dd
        
        INTEGER(I4B) :: i, ndim
        
        ndim = size(point)
        
        dd = 0
        do i = 1,ndim
            if (point(i) < this%lo%x(i)) dd = dd + ((point(i) - this%lo%x(i)) ** 2)
            if (point(i) > this%hi%x(i)) dd = dd + ((point(i) - this%hi%x(i)) ** 2)
        end do
        dd = sqrt(dd)
        
    END FUNCTION box_dist

END MODULE box01