PROGRAM KDTREEFORTRAN
    USE ifport
    USE def01
    USE interfaces01
    USE kdtree01
    USE point01
    USE hdf5
    
    IMPLICIT NONE
    
    INTEGER(HID_T) :: file_id, dataset_id, queryset_id, space_id
    
    INTEGER(HSIZE_T), DIMENSION(2) :: dataset_dims, queryset_dims, max_dims
    
    INTEGER(I4B) :: i, j, r
    TYPE(kdtree) :: kd
    TYPE(point) :: testpoint
    REAL(DP), ALLOCATABLE :: points(:, :), query(:, :)
    
    INTEGER(I4B), ALLOCATABLE :: search_point_numbers(:)
    REAL(DP), ALLOCATABLE :: search_point_distances(:)
    
    max_dims = (/ max_pts, max_dim /)
    if(.not.allocated(search_point_numbers)) allocate(search_point_numbers(1 : search_neighbors), stat = ierror)
    if(.not.allocated(search_point_distances)) allocate(search_point_distances(1 : search_neighbors), stat = ierror)
    
    ! Initialize Intel Fotran Compiler random seed.
    !call seed(1995)
    
    
    ! Initialize system clock timer.
    call SYSTEM_CLOCK(clock_count_first, clock_count_rate)
    clock_tick_time = 1.0_DP / clock_count_rate
    
    
    ! Initialize FORTRAN HDF5 interface.
    call h5open_f(ierror)
    
    
    ! Open an existing file for read only.
    call h5fopen_f(data_file, H5F_ACC_RDONLY_F, file_id, ierror)
    

    ! Open dataset, get its properties and allocate memory.
    call h5dopen_f(file_id, dataset_name, dataset_id, ierror)
    call h5dget_space_f(dataset_id, space_id, ierror)
    call h5sget_simple_extent_dims_f(space_id, dataset_dims, max_dims, ierror)
    if(.not.allocated(points)) allocate(points(1 : dataset_dims(1), 1 : dataset_dims(2)), stat = ierror)
    

    ! Open queryset, get its properties and allocate memory.
    call h5dopen_f(file_id, queryset_name, queryset_id, ierror)
    call h5dget_space_f(queryset_id, space_id, ierror)
    call h5sget_simple_extent_dims_f(space_id, queryset_dims, max_dims, ierror)
    if(.not.allocated(query)) allocate(query(1 : queryset_dims(1), 1 : queryset_dims(2)), stat = ierror)
    
    
    ! Initialize points coordinates
    !do i = 1,npts
    !    points(i, :) = npts - i + 1
    !    !do j = 1,ndim
    !    !    points(i, j) = (npts - i) * ndim + ndim + 1 - j
    !    !end do
    !end do
    

    ! Read the dataset.
    call SYSTEM_CLOCK(clock_count_first)
    call h5dread_f(dataset_id, H5T_NATIVE_DOUBLE, points, dataset_dims, ierror)
    call h5dread_f(queryset_id, H5T_NATIVE_DOUBLE, query, queryset_dims, ierror)
    call SYSTEM_CLOCK(clock_count_last)
    write (*, fmt="('Reading dataset took',F12.7,' seconds')") (clock_count_last - clock_count_first) * clock_tick_time    

    ! Close the dataset.
    call h5dclose_f(dataset_id, ierror)
    call h5dclose_f(queryset_id, ierror)
    

    ! Write points on screen.
    !do j = 1, 3
    !    write(*, *) ( points(i, j), i = 1,max_dim )
    !    write(*, *)
    !end do
    
    ! Construct KDTree.
    call SYSTEM_CLOCK(clock_count_first)
    call kdtree_init(kd, points)
    call SYSTEM_CLOCK(clock_count_last)
    write (*, fmt="('Building KD tree took',F12.7,' seconds')") (clock_count_last - clock_count_first) * clock_tick_time
    
    
    ! Search for nearest point.
    call SYSTEM_CLOCK(clock_count_first)
    do i = 1, queryset_dims(2)
        r = kdtree_nearest(kd, query(:, i))
        ! Write result to screen
        !write(*, *) query(i, :)
        !write(*, *) points(r, :)
        !write(*, *)
    end do
    call SYSTEM_CLOCK(clock_count_last)
    write (*, fmt="('Searching for one nearest point took',F12.7,' seconds')") (clock_count_last - clock_count_first) * clock_tick_time
    
    ! Search for n nearest points.
    call SYSTEM_CLOCK(clock_count_first)
    do i = 1, search_neighbors_for_points
        call kdtree_nnearest(kd, i, search_point_numbers, search_point_distances, search_neighbors)
        ! Write result to screen
        !do i = 1, search_neighbors
        !    write(*, *) points(:, search_point_numbers(i))
        !    write(*, *) search_point_distances(i)
        !    write(*, *)
        !end do
    end do
    call SYSTEM_CLOCK(clock_count_last)
    write (*, fmt="('Searching for',I,' nearest neighbors for',I,' points took',F12.7,' seconds')") search_neighbors, search_neighbors_for_points, (clock_count_last - clock_count_first) * clock_tick_time
    

    ! Search for points in radius.
    call SYSTEM_CLOCK(clock_count_first)
    do i = 1, queryset_dims(2)
        r = kdtree_locatenear(kd, query(:, i), search_point_radius, search_point_numbers, search_neighbors)
        ! Write result to screen
        !do i = 1, r
        !    write(*, *) points(point_numbers(i), :)
        !    write(*, *)
        !end do
    end do
    call SYSTEM_CLOCK(clock_count_last)
    write (*, fmt="('Searching for',I,' nearest neighbors for',I,' points in radius ',F12.7,' took',F12.7,' seconds')") search_neighbors, search_neighbors_for_points, search_point_radius, (clock_count_last - clock_count_first) * clock_tick_time
    

    ! Call kdtree_final destructor.
    call kdtree_final(kd)
    if(allocated(points)) deallocate(points, stat = ierror)
    if(allocated(query)) deallocate(query, stat = ierror)
    if(allocated(search_point_numbers)) deallocate(search_point_numbers, stat = ierror)
    if(allocated(search_point_distances)) deallocate(search_point_distances, stat = ierror)
    

    ! Close the file.
    call h5fclose_f(file_id, ierror)
    

    ! Close FORTRAN HDF5 interface.
    call h5close_f(ierror)
    
END PROGRAM KDTREEFORTRAN