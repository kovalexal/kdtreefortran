MODULE kdtree01
    USE box01
    USE def01, ONLY : DP, I4B, ierror
    USE interfaces01
    USE point01
    USE ifport
    
    IMPLICIT NONE
    
    PUBLIC :: kdtree, kdtree_init, kdtree_final, kdtree_locate, kdtree_nearest, kdtree_dist, kdtree_sift_down, kdtree_nnearest, kdtree_locatenear
    PRIVATE :: kdtree_disti, kdtree_locatei

    TYPE kdtree
        !PRIVATE

        REAL(DP) :: BIG = 1.0e99_DP ! Size of the root box, value set below.
        INTEGER(I4B) :: ndim, npts, nboxes ! Number of boxes, number of points.
        TYPE(box), ALLOCATABLE :: boxes(:) ! The array of boxnodes that form the tree.
        REAL(DP), ALLOCATABLE :: ptss(:, :)
        !INTEGER(I4B), TARGET :: ptindx(1 : size(x1)), rptindx(1 : size(x1)) ! Index if points and reverse index

        !INTEGER(I4B), ALLOCATABLE, TARGET :: ptindx(:), rptindx(:) ! Index of points and reverse index
        !REAL(DP), ALLOCATABLE, TARGET :: coords(:) ! Point coordinates, rearranged contiguously.
        INTEGER(I4B), POINTER :: ptindx(:), rptindx(:)
        REAL(DP), POINTER :: coords(:)

        CONTAINS
            ! Generic names
            !GENERIC, PUBLIC :: ether_init => ether_init1, ether_init2 ! Fortran-style overloading (name binding)
            ! Public procedures
            PROCEDURE, PUBLIC :: kdtree_init
            !FUNCTION :: kdtree_disti
            ! Destructor
            FINAL :: kdtree_final ! can be omitted
            ! Private procedures
            !PROCEDURE, PRIVATE :: ether_initsps

    END TYPE kdtree

    CONTAINS ! Begin definitions' section

    SUBROUTINE kdtree_init(this, points)
        IMPLICIT NONE
        
        CLASS(kdtree), INTENT(INOUT) :: this
        REAL(DP) :: points(:, :)
        
        INTEGER(I4B) :: jbox, k, j, m, nowtask, ntmp, idum, ptlo, pthi, tmom, tdim, np, kk
        INTEGER(I4B) :: taskmom(1 : 50), taskdim(1 : 50)
        TYPE(point) :: lo, hi
        INTEGER(I4B), POINTER :: hp(:)
        REAL(DP), POINTER :: cp(:)
        
        this%ndim = size(points, DIM = 1)
        this%npts = size(points, DIM = 2)
        
        if(.not.allocated(this%ptss)) allocate(this%ptss(1:this%ndim, 1:this%npts), stat = ierror)
        this%ptss = points
        
        if(.not.allocated(this%ptindx)) allocate(this%ptindx(1:this%npts), stat = ierror)
        if(.not.allocated(this%rptindx)) allocate(this%rptindx(1:this%npts), stat = ierror)
        
        this%ptindx = (/ (k, k=1,this%npts) /)

        ! Calculate number of boxes
        m = 1
        ntmp = this%npts
        do
            if(ntmp .eq. 0) exit
            ntmp = ishft(ntmp, -1)
            m = ishft(m, 1)
        end do
        this%nboxes = 2 * this%npts - ishft(m, -1)
        if (m < this%nboxes) this%nboxes = m
        this%nboxes = this%nboxes - 1
        
        if(.not.allocated(this%boxes)) allocate(this%boxes(1:this%nboxes), stat = ierror)
        if(.not.allocated(this%coords)) allocate(this%coords(1:(this%ndim * this%npts)), stat = ierror)
        
        ! Copy the point coordinates into contiguous array
        do k = 1, this%ndim
            this%coords(((k - 1) * this%npts + 1) : (k * this%npts)) = points(k, 1 : this%npts)
        end do
        
        ! Initialize the root box and put it on task list for subdivision
        call point_init(lo, this%ndim, -this%BIG, -this%BIG, -this%BIG, -this%BIG, -this%BIG, -this%BIG)
        call point_init(hi, this%ndim, this%BIG, this%BIG, this%BIG, this%BIG, this%BIG, this%BIG)
        call boxnode_init(this%boxes(1), lo, hi, 1, 1, 1, 1, this%npts)
        
        jbox = 1
        taskmom(2) = 1 ! Which box
        taskdim(2) = 1 ! Which dimension
        nowtask = 2
        
        do while (nowtask .ne. 1) ! Main loop over pending tasks
            tmom = taskmom(nowtask)
            tdim = taskdim(nowtask)
            nowtask = nowtask - 1

            ptlo = this%boxes(tmom)%ptlo
            pthi = this%boxes(tmom)%pthi
        
            hp => this%ptindx(ptlo : pthi) ! Points to left end of subdivision
            cp => this%coords((tdim - 1) * this%npts + 1 : (tdim - 1) * this%npts + this%npts) ! Points to coordinate list for current dim
        
            np = pthi - ptlo + 1 ! Number of points in the subdivision
            kk = (np - 1) / 2 ! Index of last point on left (boundary point)
            idum = selecti(1 + kk, hp, np, cp) ! All work is done here
        
            ! Create daughters and push them onto the task list if they need further subdivision        
            hi = this%boxes(tmom)%hi
            lo = this%boxes(tmom)%lo
        
            lo%x(tdim) = this%coords((tdim - 1) * this%npts + hp(kk + 1))
            hi%x(tdim) = lo%x(tdim)
        
            jbox = jbox + 1
            call boxnode_init(this%boxes(jbox), this%boxes(tmom)%lo, hi, tmom, 1, 1, ptlo, ptlo + kk)
        
            jbox = jbox + 1
            call boxnode_init(this%boxes(jbox), lo, this%boxes(tmom)%hi, tmom, 1, 1, ptlo + kk + 1, pthi)
        
            this%boxes(tmom)%dau1 = jbox - 1
            this%boxes(tmom)%dau2 = jbox
        
            if (kk > 1) then
                nowtask = nowtask + 1
                taskmom(nowtask) = jbox - 1
                taskdim(nowtask) = mod(tdim, this%ndim) + 1
            end if
            if (np - kk > 3) then
                nowtask = nowtask + 1
                taskmom(nowtask) = jbox
                taskdim(nowtask) = mod(tdim, this%ndim) + 1
            end if
        end do
    
        do j = 1,this%npts
            this%rptindx(this%ptindx(j)) = j
        end do
        
        if(allocated(this%coords)) deallocate(this%coords,stat=ierror)

    END SUBROUTINE kdtree_init
    
    
    FUNCTION kdtree_dist(this, point1, point2) result(dist2)
        IMPLICIT NONE
        
        TYPE(kdtree), INTENT(INOUT) :: this
        REAL(DP), INTENT(IN) :: point1(:), point2(:)
        REAL(DP) :: dist2
        
        dist2 = sqrt(sum((point1 - point2) ** 2))
        
    END FUNCTION kdtree_dist
    
    
    FUNCTION kdtree_disti(this, jpt, kpt) result(dist2)
        IMPLICIT NONE
        
        TYPE(kdtree), INTENT(INOUT) :: this
        INTEGER(I4B), INTENT(IN) :: jpt, kpt
        REAL(DP) :: dist2
        
        if (jpt .eq. kpt) then
            dist2 = this%BIG
        else
            dist2 = sqrt(sum((this%ptss(:, jpt) - this%ptss(:, kpt)) ** 2))
        end if
        
    END FUNCTION kdtree_disti
    
    FUNCTION kdtree_locate(this, point) result(index)
        IMPLICIT NONE
        
        TYPE(kdtree), INTENT(INOUT) :: this
        REAL(DP), INTENT(IN) :: point(1:this%ndim)
        INTEGER(I4B) :: index
        
        INTEGER(I4B) :: d1, jdim
        
        index = 1
        jdim = 1
        
        do while (this%boxes(index)%dau1 .ne. 1)
            d1 = this%boxes(index)%dau1
            if (point(jdim) .le. this%boxes(d1)%hi%x(jdim)) then
                index = d1
            else
                index = this%boxes(index)%dau2
            end if
            jdim = mod(jdim, this%ndim) + 1
        end do
        
    END FUNCTION kdtree_locate
    
    
    FUNCTION kdtree_locatei(this, jpt) result(nb)
        IMPLICIT NONE
        
        TYPE(kdtree), INTENT(INOUT) :: this
        INTEGER(I4B), INTENT(IN) :: jpt

        INTEGER(I4B) :: nb, d1, jh
        
        jh = this%rptindx(jpt)
        nb = 1

        do while(this%boxes(nb)%dau1 .ne. 1)
            d1 = this%boxes(nb)%dau1
            if (jh .le. this%boxes(d1)%pthi) then
                nb = d1
            else
                nb = this%boxes(nb)%dau2
            end if
        end do
        
    END FUNCTION kdtree_locatei
    
    
    FUNCTION kdtree_nearest(this, point) result(nrst)
        IMPLICIT NONE
        
        TYPE(kdtree), INTENT(INOUT) :: this
        REAL(DP), INTENT(IN) :: point(1:this%ndim)
        INTEGER(I4B) :: nrst
        
        INTEGER(I4B) :: i, k, ntask
        INTEGER(I4B) :: task(1:50)
        
        REAL(DP) :: dnrst, d
        
        dnrst = this%BIG
        k = kdtree_locate(this, point)
        
        do i = this%boxes(k)%ptlo, this%boxes(k)%pthi
            d = kdtree_dist(this, this%ptss(:, this%ptindx(i)), point)
            if (d .lt. dnrst) then
                nrst = this%ptindx(i)
                dnrst = d
            end if
        end do
        
        task(2) = 1
        ntask = 2
        
        do while (ntask .ne. 1)
            k = task(ntask)
            ntask = ntask - 1
            
            if (box_dist(this%boxes(k), point) < dnrst) then
                if (this%boxes(k)%dau1 .ne. 1) then
                    ntask = ntask + 1
                    task(ntask) = this%boxes(k)%dau1
                    
                    ntask = ntask + 1
                    task(ntask) = this%boxes(k)%dau2
                else
                    do i = this%boxes(k)%ptlo, this%boxes(k)%pthi
                        d = kdtree_dist(this, this%ptss(:, this%ptindx(i)), point)
                        
                        if (d .lt. dnrst) then
                            nrst = this%ptindx(i)
                            dnrst = d
                        end if
                    end do
                end if
            end if
        end do
                
    END FUNCTION kdtree_nearest
    
    
    SUBROUTINE kdtree_sift_down(this, heap, ndx, nn)
        IMPLICIT NONE
        TYPE(kdtree), INTENT(INOUT) :: this
        REAL(DP) :: heap(:)
        INTEGER(I4B) :: ndx(:)
        INTEGER(I4B) :: nn
        
        INTEGER(I4B) :: n, j, jold, ia
        REAL(DP) :: a
        
        n = nn
        a = heap(1)
        ia = ndx(1)
        jold = 1
        j = 2
        
        do while (j .le. n)
            if (j .lt. n) then
                if (heap(j) .lt. heap(j + 1)) j = j + 1
            end if
            if (a .ge. heap(j)) exit
            
            heap(jold) = heap(j)
            ndx(jold) = ndx(j)
            
            jold = j
            j = 2 * j ! + 1
        end do
        
        heap(jold) = a
        ndx(jold) = ia
    
    END SUBROUTINE kdtree_sift_down
    
    
    SUBROUTINE kdtree_nnearest(this, jpt, nn, dn, n)
        IMPLICIT NONE
        TYPE(kdtree), INTENT(INOUT) :: this
        INTEGER(I4B) :: jpt
        INTEGER(I4B) :: nn(:)
        REAL(DP) :: dn(:)
        INTEGER(I4B) :: n
        
        INTEGER(I4B) :: i, k, ntask, kp
        INTEGER(I4B) :: task(1 : 50)
        REAL(DP) :: d
        
        if (n .gt. this%npts) then
            write(*, *) "too many neighbors requested"
            stop 0
        end if
        
        dn = this%BIG
        kp = this%boxes(kdtree_locatei(this, jpt))%mom
        
        do while((this%boxes(kp)%pthi - this%boxes(kp)%ptlo) .lt. n)
            kp = this%boxes(kp)%mom
        end do
        
        do i = this%boxes(kp)%ptlo, this%boxes(kp)%pthi
            if (jpt .eq. this%ptindx(i)) cycle
            
            d = kdtree_disti(this, this%ptindx(i), jpt)
            
            if (d .lt. dn(1)) then
                dn(1) = d
                nn(1) = this%ptindx(i)
                if (n .gt. 1) call kdtree_sift_down(this, dn, nn, n)
            end if
        end do
        
        task(2) = 1
        ntask = 2
        
        do while(ntask .ne. 1)
            k = task(ntask)
            ntask = ntask - 1
            
            if (k .eq. kp) cycle
            
            if (box_dist(this%boxes(k), this%ptss(:, jpt)) .lt. dn(1)) then
                if (this%boxes(k)%dau1 .ne. 1) then
                    ntask = ntask + 1
                    task(ntask) = this%boxes(k)%dau1
                    
                    ntask = ntask + 1
                    task(ntask) = this%boxes(k)%dau2
                
                else
                    do i = this%boxes(k)%ptlo, this%boxes(k)%pthi
                        d = kdtree_disti(this, this%ptindx(i), jpt)
                        
                        if (d .lt. dn(1)) then
                            dn(1) = d
                            nn(1) = this%ptindx(i)
                            
                            if (n .gt. 1) call kdtree_sift_down(this, dn, nn, n)
                            
                        end if
                        
                    end do
                    
                end if
            end if
        
        end do
    
    END SUBROUTINE kdtree_nnearest
    

    SUBROUTINE kdtree_final(this) ! definition of the destructor
        IMPLICIT NONE
        TYPE(kdtree), INTENT(INOUT) :: this
        
        if(allocated(this%ptss)) deallocate(this%ptss,stat=ierror)
        if(allocated(this%ptindx)) deallocate(this%ptindx,stat=ierror)
        if(allocated(this%rptindx)) deallocate(this%ptindx,stat=ierror)
        if(allocated(this%boxes)) deallocate(this%boxes,stat=ierror)
    
        ! if(allocated(this%x)) deallocate(this%x,stat=ierror)
    
    END SUBROUTINE kdtree_final
    
    
    FUNCTION kdtree_locatenear(this, point, r, list, nmax) result(nret)
        IMPLICIT NONE
        
        TYPE(kdtree), INTENT(INOUT) :: this
        REAL(DP) :: point(:)
        REAL(DP) :: r
        INTEGER(I4B) :: list(:)
        INTEGER(I4B) :: nmax
        
        INTEGER(I4B) :: k, i, nb, nbold, nret, ntask, jdim, d1, d2
        INTEGER(I4B) :: task(1 : 50)
        
        nret = 0
        jdim = 1
        nb = jdim
        
        if (r .lt. 0.0_DP) then
            write(*, *) "radius must be nonnegative"
            stop 0
        end if
     
        do while(this%boxes(nb)%dau1 .ne. 1)
            nbold = nb
            d1 = this%boxes(nb)%dau1
            d2 = this%boxes(nb)%dau2
            
            if (point(jdim) + r .le. this%boxes(d1)%hi%x(jdim)) then
                nb = d1
            else if (point(jdim) - r .ge. this%boxes(d2)%lo%x(jdim)) then
                nb = d2
            end if
            jdim = mod(jdim, this%ndim) + 1
            
            if (nb .eq. nbold) exit
        end do
        
        task(2) = nb
        ntask = 2
        
        do while(ntask .ne. 1)
            k = task(ntask)
            ntask = ntask - 1
            
            if (box_dist(this%boxes(k), point) .gt. r) cycle
            
            if (this%boxes(k)%dau1 .ne. 1) then
                ntask = ntask + 1
                task(ntask) = this%boxes(k)%dau1
                
                ntask = ntask + 1
                task(ntask) = this%boxes(k)%dau2
                
            else
                do i = this%boxes(k)%ptlo, this%boxes(k)%pthi
                    if ((kdtree_dist(this, this%ptss(:, this%ptindx(i)), point) .le. r) .and. (nret .lt. nmax)) then
                        list(nret + 1) = this%ptindx(i)
                        nret = nret + 1
                    end if
                    if (nret .eq. nmax) return
                end do
                
            end if
        end do
        
    END FUNCTION kdtree_locatenear
    
    !SUBROUTINE box_final(this) ! definition of the destructor
    !  IMPLICIT NONE
    !  TYPE(box), INTENT(INOUT) :: this
    !
    !  if(allocated(this%x)) deallocate(this%x,stat=ierror)
    !
    !END SUBROUTINE ether_final

END MODULE kdtree01