MODULE def01
    
! Written by F.S. Zaitsev 04.07.2015.
! Last modification 04.07.2015.
    IMPLICIT NONE
 
    INTEGER, PARAMETER :: SP = KIND(1.0)
    INTEGER, PARAMETER :: DP = KIND(1.0d+00)  ! If DP is changed then change interface with C++
    INTEGER, PARAMETER :: I4B = SELECTED_INT_KIND(9)  ! Represents -10**9<n<10**9. If I4B is changed then change interface with C++
    INTEGER, PARAMETER :: LGT = KIND(.true.)

    ! Set of points
    !REAL(DP), ALLOCATABLE :: x1(:), x2(:), x3(:), x4(:), x5(:), x6(:)  ! coordinates of points
    INTEGER(I4B), ALLOCATABLE :: indx(:)
    INTEGER(I4B) :: max_pts = 10000000, max_dim = 6, search_neighbors = 10000, search_neighbors_for_points = 10000
    REAL(DP) :: search_point_radius = 3000.0_DP
  
    ! Subsidary objects:
    INTEGER :: ierror  ! allocation status
    REAL(DP) :: clock_tick_time
    INTEGER(8) :: clock_count_first, clock_count_last, clock_count_rate
    
    ! Constants
    REAL(DP), PARAMETER :: PIVAL = 3.141592653589793e+00_DP
    REAL(DP) :: &
    pi = PIVAL, &
    pi05 = 0.5e+00_DP * PIVAL, &
    pi2 = 2.0e+00_DP * PIVAL, &
    pi1p5 = 1.5e+00_DP * PIVAL, &
    pi3 = 3.0e+00_DP * PIVAL, &
    pi4 = 4.0e+00_DP * PIVAL, &
    pi8 = 8.0e+00_DP * PIVAL, &
    onedpi8 = 1.0e+00_DP / (8.0e+00_DP * PIVAL), &
    pi2pi2 = 2.0e+00_DP * PIVAL * 2.0e+00_DP * PIVAL

    ! Input and output parameters
    INTEGER :: io = 4, ioerr = 8, iowrk = 3, iowrk1 = 9, iowrk2 = 10, iowrk3 = 11
    CHARACTER *128 :: fout = 'result', ferr = 'error', fname
    CHARACTER *128 :: infile, targetch, dummych, &
    indir = '../.././DATA.IN', &
    outdir = '../.././DATA.OUT', &
    data_file = 'dataset.hdf5', &
    dataset_name = 'dataset', &
    queryset_name = 'query'
    INTEGER :: iverb = 0  ! 0 - reduced output, > 0 - verbose output
    
END MODULE def01

  
MODULE interfaces01
    INTERFACE
        FUNCTION selecti(k,indx,n,arr)
            ! Written by F.S. Zaitsev 03.07.2015.
            ! Last modification 03.07.2015.
            USE def01, ONLY : DP,I4B
            IMPLICIT NONE
            INTEGER(I4B), INTENT(IN) :: k,n
            INTEGER(I4B) :: indx(:)
            REAL(DP) :: arr(:)
            INTEGER(I4B) :: selecti  
        END FUNCTION selecti
    END INTERFACE

    INTERFACE 
        SUBROUTINE KDtreebuild(ndim,x1,x2,x3)
            ! Written by F.S. Zaitsev 03.07.2015.
            ! Last modification 03.07.2015.
            USE def01, ONLY : DP,I4B
            IMPLICIT NONE
            INTEGER(I4B), INTENT(IN) :: ndim  ! number od dimensions
            REAL(DP) :: x1(:),x2(:),x3(:)
        END SUBROUTINE KDtreebuild
    END INTERFACE
      
END MODULE interfaces01