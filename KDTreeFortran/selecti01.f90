FUNCTION selecti(k, indx, n, arr)
    USE def01, ONLY : DP, I4B
    IMPLICIT NONE
        
    INTEGER(I4B), INTENT(IN) :: k, n
    INTEGER(I4B) :: indx(:)
    REAL(DP) :: arr(:)
    INTEGER(I4B) :: selecti
        
    INTEGER(I4B) :: i,ia,ir,j,l,mid,tmp
    REAL(DP) :: a

    !FSZ l=0
    !FSZ ir=n-1
    l=1
    ir=n
    do 
        if (ir .le. (l + 1)) then
            if ((ir .eq. (l + 1)) .and. (arr(indx(ir)) .lt. arr(indx(l)))) then
                tmp = indx(l)
                indx(l) = indx(ir)
                indx(ir) = tmp
            end if
        
            selecti = indx(k)
            return
        else
            mid = ishft(l + ir, -1)
            tmp = indx(mid)
            indx(mid) = indx(l + 1)
            indx(l + 1)=tmp
        
            if(arr(indx(l)) > arr(indx(ir))) then
                tmp = indx(l)
                indx(l) = indx(ir)
                indx(ir) = tmp
            end if
        
  	        if(arr(indx(l + 1)) > arr(indx(ir))) then
                tmp =indx(l + 1)
                indx(l + 1) = indx(ir)
                indx(ir) = tmp
            end if
        
  	        if(arr(indx(l)) > arr(indx(l + 1))) then
                tmp = indx(l)
                indx(l) = indx(l + 1)
                indx(l + 1) = tmp
            end if
        
  	        i = l + 1
  	        j = ir
  	        ia = indx(l + 1)
  	        a = arr(ia)
  	        do
  		        do
                    i = i + 1
                    if(arr(indx(i)) >= a) exit
                end do
  		        do
                    j = j - 1
                    if(arr(indx(j)) <= a) exit
                end do
            
  		        if (j < i) exit
            
                tmp = indx(i)
                indx(i) = indx(j)
                indx(j) = tmp        
            end do
        
  	        indx(l + 1) = indx(j)
  	        indx(j) = ia
  	        if (j >= k) ir = j - 1
  	        if (j <= k) l = i
        end if
    end do
        
END FUNCTION selecti