MODULE point01
    ! Ether class.
    ! Written by F.S. Zaitsev 06.11.2014.
    ! Last modification 06.11.2014.
    USE def01, ONLY : DP, I4B, ierror
    IMPLICIT NONE

    TYPE point
        !PRIVATE
        
        REAL(DP), ALLOCATABLE :: x(:)

        CONTAINS
            ! Generic names
            !GENERIC, PUBLIC :: ether_init => ether_init1, ether_init2 ! Fortran-style overloading (name binding)
            ! Public procedures
            PROCEDURE, PUBLIC :: point_init
            ! Destructor
            !FINAL :: point_final ! can be omitted
            ! Private procedures
            !PROCEDURE, PRIVATE :: ether_initsps

    END TYPE point


    CONTAINS ! Begin definitions' section

    SUBROUTINE point_init(this, ndim, x1, x2, x3, x4, x5, x6)
        ! Initialize ether.
        ! Written by F.S. Zaitsev 06.11.2014.
        ! Last modification 06.11.2014.
        !USE def01, ONLY : I4B,DP,pi2
        IMPLICIT NONE
        CLASS(point), INTENT(INOUT) :: this
        INTEGER(I4B), INTENT(IN) :: ndim
        REAL(DP), INTENT(IN) :: x1, x2, x3, x4, x5, x6

        if(.not.allocated(this%x)) allocate(this%x(1 : ndim), stat = ierror)
        this%x(1) = x1
        if (ndim > 1) this%x(2) = x2
        if (ndim > 2) this%x(3) = x3
        if (ndim > 3) this%x(4) = x4
        if (ndim > 4) this%x(5) = x5
        if (ndim > 5) this%x(6) = x6
        if (ndim > 6) write(*,*) 'Point not implemented for DIM > 6'

    END SUBROUTINE point_init


    !SUBROUTINE point_final(this) ! definition of the destructor
    !    IMPLICIT NONE
    !    TYPE(point), INTENT(INOUT) :: this
    !
    !    if(allocated(this%x)) deallocate(this%x,stat=ierror)
    !
    !END SUBROUTINE ether_final

END MODULE point01