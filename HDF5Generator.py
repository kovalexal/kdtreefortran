#!/usr/bin/env python2
import json, h5py
import numpy as np

POINTS_VALUES_RANGE = (-10000, 10000)
POINTS_DIM = 6
DATASET_POINTS = 100000
QUERY_POINTS = 10000
hdf5_output_file = 'dataset.hdf5'
json_output_file = 'dataset.json'
csv_output_file = 'dataset.csv'

# Generating points to dataset
print('Generating {0} dataset points with dim {1}'.format(DATASET_POINTS, POINTS_DIM))
dataset_points = np.random.uniform(low = POINTS_VALUES_RANGE[0], high = POINTS_VALUES_RANGE[1], size = (DATASET_POINTS, POINTS_DIM))
print('Generating {0} query points with dim {1}'.format(QUERY_POINTS, POINTS_DIM))
query_points = np.random.uniform(low = POINTS_VALUES_RANGE[0], high = POINTS_VALUES_RANGE[1], size = (QUERY_POINTS, POINTS_DIM))

# Dump points to HDF5 file
with h5py.File(hdf5_output_file, 'w') as f:
    print('Dumping points to {0}'.format(hdf5_output_file))

    dataset = f.create_dataset('dataset', (DATASET_POINTS, POINTS_DIM), dtype='f')
    dataset[...] = dataset_points
    query = f.create_dataset('query', (QUERY_POINTS, POINTS_DIM), dtype='f')
    query[...] = query_points

    # We can save in this way, but cannot provide additional parameters to dataset
    #f['dataset'] = dataset_points
    #f['query'] = query_points

    print('Wrote dataset to {0}'.format(hdf5_output_file))

# Dump points to JSON file
# with open(json_output_file, 'w') as f:
#     print('Dumping points to {0}'.format(json_output_file))

#     datadict = {
#         'dataset' : dataset_points.tolist(),
#         'query' : query_points.tolist(),
#     }

#     f.write(json.dumps(datadict, separators=(',', ':')))
#     print('Wrote dataset to {0}'.format(json_output_file))

# Dump points to CSV format
# with open(csv_output_file, 'w') as f:
#     print('Dumping points to {0}'.format(csv_output_file))
#     for point in dataset_points:
#         f.write(','.join(str(x) for x in point) + '\n')

#     f.write('\n')

#     for point in query_points:
#         f.write(','.join(str(x) for x in point) + '\n')

#     print('Wrote dataset to {0}'.format(csv_output_file))